FROM node:14.17-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . ./

CMD ["npm", "run", "start"]

# FROM node:14.17-alpine as build

# WORKDIR /app
# COPY package*.json ./
# RUN npm install
# COPY . ./
# RUN npm run build

# FROM node:14.17-alpine as server

# WORKDIR /app
# COPY package*.json ./
# RUN npm install --production
# COPY --from=build ./app/public ./public
# COPY --from=build ./app/dist ./dist

# CMD ["npm", "run", "production"]
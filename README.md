# README #

This project is an API sample that has been developed using Node.js, Express, Typescript, Docker, PostgreSQL for the Infosel Challenge

### How do I get set up? ###

## Requirements: 
* Node.js version >= 14.17.1
* Docker >= 20.10.7

## How to run with Docker? ###
1. Run Docker on your SO
2. In the Docker's Settings, make sure that at least your project's directory is added as a shared resource in `Resources > File Sharing`. 
(More info: https://docs.docker.com/desktop/windows/#resources)
3. Open CMD window and move into the project's root folder 
4. Run `npm run docker` or `docker compose up --build`
5. Open http://localhost:8000/docs (By default the port used by the app is the 8000, to change the port update it in the .env file)
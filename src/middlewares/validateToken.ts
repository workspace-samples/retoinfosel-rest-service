import { Request, Response, NextFunction } from "express";
import jwt from 'jsonwebtoken';

export const extractToken = (headers: any) => {
  const authHeader = headers.authorization;
  return authHeader!.split(' ')[1];
}

const validateToken = async (
    request: Request,
    response: Response,
    next: NextFunction
  ) => {
    try {
      const token = extractToken(request.headers)
      jwt.verify(token, process.env.TOKEN_SECRET!, (error: any, decoded: any) => {
          if (error) return response.status(403).send("Invalid Token");
          return next();
      });
    } catch (e) {
      console.log(e)
      return response.status(403).send("Invalid Token");
    }
    return null;
  };

export default validateToken;
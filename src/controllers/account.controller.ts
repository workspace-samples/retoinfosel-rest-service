import { Route, Tags, Post, Body, Get, Path } from "tsoa";
import { Account, IAccount } from "../models";
import { createAccount, getAccountsByClient } from "../services/account.service";


@Route("account")
@Tags("Account")
export default class AccountController {
  @Post("/")
  public async createAccount(@Body() body: { clientId: number, productId: number }): Promise<IAccount | null> {
    return createAccount(body);
  }

  @Get("/client/:id")
  public async getAccountsByClient(@Path() id: string): Promise<Array<IAccount>> {
    return getAccountsByClient(Number(id));
  }
}


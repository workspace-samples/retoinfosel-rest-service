import { Route, Tags, Post, Body, Get, Path } from "tsoa";
import { Transaction } from "../models";
import { createTransaction, updateTransaction } from "../services/transaction.service";
import { updateBalance } from "../services/account.service";
import { checkUserToken} from "../services/user.service";
import { extractToken } from "../middlewares/validateToken"
import { ITransaction } from "../models/transaction.model";

@Route("transaction")
@Tags("Transaction")
export default class TransactionController {
  @Post("/")
  public async createTransaction(@Body() payload: { body: ITransaction, headers: any }): Promise<ITransaction | null> {
    const { body, headers} = payload
    const token = extractToken(headers)
    let user = await checkUserToken({ id: body.userId, token })
    if(!user) return null;
    let transaction = await createTransaction(body);
    if(!transaction) return null;
    transaction = await updateBalance(transaction)
    if(!transaction) return null;
    return updateTransaction({ id: transaction.id, transactionStatusId: 2})
  }
}


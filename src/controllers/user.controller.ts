import { Route, Tags, Body, Delete, Post} from "tsoa";
import { IUserSession } from "../models"
import { deleteUser, loginUser, updateUser } from "../services/user.service";


@Route("user")
@Tags("User")
export default class UserController {

  @Post("/session")
  public async loginUser(@Body() body: { username: string, password: string }): Promise<IUserSession | null> {
    const sessionData = await loginUser(body)
    if(!sessionData) return null;
    const user = updateUser({id: sessionData.userId, token: sessionData.token})
    if(!user) return null;
    return sessionData
  }

  @Delete("/")
  public async deleteUser(@Body() body: { id: number }): Promise<{ message: string } | null> {
    return deleteUser(body)
  }
}


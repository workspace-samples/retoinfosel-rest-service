import { Route, Tags, Post, Body, Patch} from "tsoa";
import {  IAccount, IClient, Product } from "../models";
import { omit } from "lodash";
import {
  createClient,
  updateClient
} from "../services/client.service";

import {
  createUser,
} from "../services/user.service";
import { createAccount } from "../services/account.service";


@Route("client")
@Tags("Client")
export default class ClientController {
  @Post("/")
  public async createClient(@Body() body: { client: IClient, productId: number, username: string, password: string}): Promise<{client: IClient, userId: number, username: string, account: IAccount} | null> {
    let client = await createClient(body.client)
    if(!client) return null;
    let user = await createUser({ client, username: body.username, password: body.password});
    console.log(user)
    if(!user) return null;
    let account = await createAccount({ clientId: client.id, productId: body.productId});
    if(!account) return null;
    return {  client, userId: user.id, username: body.username, account }
  }

  @Patch("/")
  public async updateClient(@Body() body: {id: number, phone: string, address: string}): Promise<IClient | null> {
    return updateClient(body)
  }
}


import express, { Application } from "express";
import helmet from "helmet";
import cors from "cors";
import morgan from "morgan";
import swaggerUi from "swagger-ui-express";
import "reflect-metadata";
import { createConnection } from "typeorm";
import dbConfig from "./config/database";
import { RouterAccount, RouterClient, RouterTransaction, RouterUser } from "./routes";

const APP_PORT = process.env.APP_PORT;

const app: Application = express();

app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(morgan("tiny"));
app.use(express.static("public"));

app.use(
  `/docs`,
  swaggerUi.serve,
  swaggerUi.setup(undefined, {
    swaggerOptions: {
      url: "/swagger.json",
    },
  })
);

app.use(`/client`, RouterClient);
app.use(`/account`, RouterAccount);
app.use(`/user`, RouterUser);
app.use(`/transaction`, RouterTransaction);

createConnection(dbConfig)
  .then((_connection) => {
    app.listen(APP_PORT, () => {
      console.log("Server is running on port", APP_PORT);
    });
  })
  .catch((err) => {
    console.log("Unable to connect to db", err);
    process.exit(1);
  });

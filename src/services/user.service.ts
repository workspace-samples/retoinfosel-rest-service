import { getRepository } from "typeorm";
import { User, IUser, IUserSession } from "../models";
import jwt from 'jsonwebtoken';
import bcrypt from "bcrypt";

export const createUser = async (payload: IUser): Promise<User> => {
  const userRepository = getRepository(User);
  const user = new User();
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(payload.password, salt);
  const new_payload = {...payload, password: hash}
  return userRepository.save({
    ...user,
    ...new_payload
  });
};

export const deleteUser = async (payload: {id: number}): Promise<{ message: string } | null> => {
  const userRepository = getRepository(User);
  const result = await userRepository.createQueryBuilder().update(User).set({ status: false }).where("clientId = :id", { id: payload.id}).execute();
  if(result.affected == 0) return null
  return { message: "User has been deactivated"}
};

export const loginUser = async (payload: any): Promise<IUserSession | null> => {
  const userRepository = getRepository(User);
  const user = await userRepository.find({ where: { username: payload.username } })
  if(!user) return null;
  if(!await comparePassword(payload.password, user[0].password)) return null;
  const token = jwt.sign({ username: payload.username}, process.env.TOKEN_SECRET!);
  return { token, userId: user[0].id, username: payload.username, clientId: user[0].clientId }
}

export const updateUser = async (payload: any): Promise<User | null> => {
  const userRepository = getRepository(User);
  const result = await userRepository.update({ id: payload.id }, { ...payload })
  if(result.affected == 0) return null
  const user = await userRepository.findOne({ id: payload.id })
  if(!user) return null
  return user
};

export const checkUserToken = async (payload: any): Promise<any | null> => {
  const userRepository = getRepository(User);
  console.log(payload);
  const user = await userRepository.find({ where: { id: payload.id, token: payload.token} })
  console.log(user);
  if(!user[0]) return null;
  return user[0]
}

const comparePassword = async(
  password: string,
  cryptedPassword: string
) => {
  console.log(password);
  console.log(cryptedPassword)
  return bcrypt.compare(password, cryptedPassword).catch(() => false);
};
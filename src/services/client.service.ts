import { getRepository } from "typeorm";
import { Client, IClient } from "../models";

export const createClient = async (payload: IClient): Promise<Client> => {
  const clientRepository = getRepository(Client);
  const client = new Client();
  return clientRepository.save({
    ...client,
    ...payload,
  });
};

export const updateClient = async (payload: {id: number, phone: string, address: string}): Promise<Client | null> => {
  const clientRepository = getRepository(Client);
  const result = await clientRepository.update({ id: payload.id }, {
    address: payload.address,
    phone: payload.phone
  })
  if(result.affected == 0) return null
  const client = await clientRepository.findOne({ id: payload.id })
  if(!client) return null
  return client
};
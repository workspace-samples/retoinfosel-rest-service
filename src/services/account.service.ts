import { getRepository } from "typeorm";
import {
  Account,
  Client,
  IAccount,
  User,
  TransactionType,
  Transaction,
} from "../models";
import { v4 as uuidv4 } from "uuid";

export const createAccount = async (payload: {clientId: number, productId: number}): Promise<Account> => {
  const accountRepository = getRepository(Account);
  const account = new Account();
  const uuid = uuidv4();
  const nip = Math.floor(1000 + Math.random() * 9000).toString();
  const newPayload = {
    ...payload,
    account: uuid,
    nip,
  };
  return accountRepository.save({
    ...account,
    ...newPayload,
  });
};

export const getAccountsByClient = async (
  id: number
): Promise<Array<IAccount>> => {
  const accountRepository = getRepository(Account);
  const accounts = accountRepository
    .createQueryBuilder("account")
    .leftJoinAndSelect(Client, "client", `"client"."id" = "account"."clientId"`)
    .leftJoinAndSelect(User, "user", `"user"."clientId" = "client"."id"`)
    .where(`"account"."clientId" = :clientId and "user"."status" = true`, {
      clientId: id,
    })
    .getMany();
  return accounts;
};

export const updateBalance = async (payload: Transaction): Promise<any> => {
  let { transactionTypeId, originAccountId, destinationAccountId, amount } = payload;
  const transactionTypeRepository = getRepository(TransactionType);
  const transactionType = await transactionTypeRepository.findOne({
    id: Number(transactionTypeId),
  });
  if (!transactionType) return null;
  const accountRepository = getRepository(Account);
  const originAccount = await accountRepository.findOne({ id: originAccountId });
  if (!originAccount) return null;

  let balance = Number(originAccount.balance);
  switch (transactionType.name.toLowerCase()) {
    case "cargo":
      const destinationAccount = await accountRepository.findOne({
        id: destinationAccountId,
      });
      if(!destinationAccount) return null;
      const tempBalance = Number(destinationAccount.balance) + Number(amount);
      const result = await accountRepository.update({ id: destinationAccount.id }, { balance: tempBalance });
      if(result.affected == 0) return null
      balance =  balance - Number(amount)
      break;
    case "abono":
      balance =  balance + Number(amount)
      break;
  }

  const result = await accountRepository.update({ id: originAccount.id }, { balance });
  if(result.affected == 0) return null
  return payload
};

import { getRepository } from "typeorm";
import { Transaction } from "../models";
import { ITransaction } from "../models/transaction.model";

export const createTransaction = async (payload: any): Promise<Transaction> => {
  const transactionRepository = getRepository(Transaction);
  const transaction = new Transaction();
  return transactionRepository.save({
    ...transaction,
    ...payload
  });
};

export const updateTransaction = async (payload: any): Promise<any> => {
    const transactionRepository = getRepository(Transaction);
    const result = await transactionRepository.update({ id: payload.id }, { ...payload })
    if(result.affected == 0) return null
    const transaction = await transactionRepository.findOne({ id: payload.id })
    if(!transaction) return null
    return transaction
};
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { Account } from "."

export interface IClient {
    id?: number;
    name: string;
    lastname: string;
    email: string;
    phone: string;
    rfc: string;
    address: string;
}

@Entity()
export class Client {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @Column()
  lastname!: string;

  @Column({
    unique: true
  })
  email!: string;

  @Column({
    type: "varchar",
    length: 12
  })
  phone!: string;

  @Column({ 
    type: "varchar",
    length: 13,
    unique: true
  })
  rfc!: string;

  @Column({
    type: "text"
  })
  address!: string;
  
  @OneToMany(() => Account, account => account.client)
  accounts?: Account[];
  
}

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
} from "typeorm";
import { Client } from ".";

export interface IUser {
  username: string;
  password: string;
  client: Client;
}

export interface IUserSession {
  userId: number;
  clientId: number;
  username: string;
  token: string;
}

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ unique: true })
  username!: string;

  @Column()
  password!: string;

  @Column({
    nullable: true,
    type: "text",
  })
  token!: string;

  @Column({
    default: "true",
  })
  status!: boolean;

  @OneToOne(() => Client)
  @JoinColumn()
  client?: Client;

  @Column()
  clientId!: number;
}

import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class TransactionOperation {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;
}

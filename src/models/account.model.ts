import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from "typeorm";
import { Client, Product } from ".";

export interface IAccount {
  id: number;
  account: string;
  balance: number;
  nip: string;
  productId: number;
}

@Entity()
export class Account {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  account!: string;

  @Column({
    default: 0.0,
    type: "decimal",
    precision: 18,
    scale: 2,
  })
  balance!: number;

  @Column({
    type: "varchar",
    length: 4,
  })
  nip!: string;

  @Column({
    default: "true",
  })
  status!: boolean;

  @ManyToOne(() => Product, (product) => product.id)
  @JoinColumn()
  product?: Product;

  @Column()
  productId!: number;

  @ManyToOne(() => Client, (client) => client.accounts)
  @JoinColumn()
  client!: Client;

  @Column()
  clientId!: number;
}

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  JoinColumn,
  ManyToOne,
  OneToOne,
  UpdateDateColumn,
} from "typeorm";
import {
  Account,
  TransactionOperation,
  TransactionStatus,
  TransactionType,
  User,
} from ".";

export interface ITransaction {
  id?: number;
  amount: number;
  userId: number;
  reference: string;
  description: string;
  originAccountId: number;
  destinationAccountId: number;
  transactionOperationId: number;
  transactionTypeId: number;
  transactionStatusId: number;
}

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({
    default: 0.0,
    type: "decimal",
    precision: 18,
    scale: 2,
  })
  amount!: number;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: "userId" })
  user?: User;

  @Column()
  userId!: number;

  @Column()
  reference!: string;

  @Column()
  description!: string;

  @ManyToOne(() => Account, (originAccount) => originAccount.id)
  @JoinColumn({ name: "originAccountId" })
  originAccount?: Account;

  @Column()
  originAccountId!: number;

  @ManyToOne(() => Account, (destinationAccount) => destinationAccount.id)
  @JoinColumn({ name: "destinationAccountId" })
  destinationAccount?: Account;

  @Column()
  destinationAccountId!: number;

  @ManyToOne(() => TransactionType, (transactionType) => transactionType.id)
  @JoinColumn({ name: "transactionTypeId" })
  transactionType!: TransactionType;

  @Column()
  transactionTypeId!: string;

  @ManyToOne(
    () => TransactionStatus,
    (transactionStatus) => transactionStatus.id
  )
  @JoinColumn({ name: "transactionStatusId" })
  transactionStatus!: TransactionStatus;

  @Column()
  transactionStatusId!: number;

  @ManyToOne(
    () => TransactionOperation,
    (transactionOperation) => transactionOperation.id
  )
  @JoinColumn({ name: "transactionOperationId" })
  transactionOperation!: Transaction;

  @Column()
  transactionOperationId!: number;

  @UpdateDateColumn()
  effectiveAt!: Date;
}

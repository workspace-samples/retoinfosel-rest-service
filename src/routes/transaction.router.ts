import express, { Request, Response} from "express";
import TransactionController from "../controllers/transaction.controller";
import validateToken from "../middlewares/validateToken";
const router = express.Router();

router.post("/", validateToken, async (request: Request, response: Response) => {
  const controller = new TransactionController();
  const result = await controller.createTransaction(request);
  return response.send(result);
});

export default router;
import express, { Request, Response} from "express";
import ClientController from "../controllers/client.controller";
import validateToken from "../middlewares/validateToken";

const router = express.Router();

router.post("/", async (request: Request, response: Response) => {
  const controller = new ClientController();
  const client = await controller.createClient(request.body);
  return response.send(client);
});

router.patch("/", validateToken, async (request: Request, response: Response) => {
  const controller = new ClientController();
  const client = await controller.updateClient(request.body);
  return response.send(client);
});

export default router;
import express, { Request, Response} from "express";
import AccountController from "../controllers/account.controller";
import validateToken from "../middlewares/validateToken";

const router = express.Router();

router.post("/", validateToken, async (request: Request, response: Response) => {
  const controller = new AccountController();
  const account = await controller.createAccount(request.body);
  return response.send(account);
});

router.get("/client/:id", validateToken, async (request: Request, response: Response) => {
  const controller = new AccountController();
  const accounts = await controller.getAccountsByClient(request.params.id);
  if (!accounts) response.status(404).send({ message: "No accounts found" });
  return response.send(accounts);
});

export default router;
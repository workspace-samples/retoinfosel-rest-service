import express, { Request, Response} from "express";
import UserController from "../controllers/user.controller";
import validateToken from "../middlewares/validateToken";

const router = express.Router();

router.post("/session", async (request: Request, response: Response) => {
  const controller = new UserController();
  const result = await controller.loginUser(request.body);
  return response.send(result);
});

router.delete("/", validateToken, async (request: Request, response: Response) => {
  const controller = new UserController();
  const result = await controller.deleteUser(request.body);
  return response.send(result);
});

export default router;
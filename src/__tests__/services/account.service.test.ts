import { getRepository } from "typeorm";
import { mocked } from "ts-jest/utils";
import * as AccountService from "../../services/account.service";
import { Account, Client, User } from "../../models";

jest.mock("typeorm", () => {
  return {
    getRepository: jest.fn().mockReturnValue({
      createQueryBuilder: jest.fn(() => ({})),
      save: jest.fn(),
    }),
    PrimaryGeneratedColumn: jest.fn(),
    Column: jest.fn(),
    Entity: jest.fn(),
    OneToOne: jest.fn(),
    ManyToOne: jest.fn(),
    OneToMany: jest.fn(),
    JoinColumn: jest.fn(),
    CreateDateColumn: jest.fn(),
    UpdateDateColumn: jest.fn(),
  };
});

const mockedGetRepo = mocked(getRepository(<jest.Mock>{}));

beforeEach(() => {
  mockedGetRepo.createQueryBuilder.mockClear();
});

describe("AccountService", () => {
  describe("getAccountsByClient", () => {
    test("Should return empty array", async () => {
      const id = 0;
      const accountsData: Account[] = [];
      mockedGetRepo.createQueryBuilder = jest.fn().mockReturnValue({
        where: jest.fn().mockReturnThis(),
        leftJoinAndSelect: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockResolvedValue(accountsData),
      });
      const accounts = await AccountService.getAccountsByClient(id);
      expect(accounts).toEqual(accountsData);
      expect(
        mockedGetRepo.createQueryBuilder().leftJoinAndSelect
      ).toHaveBeenCalledTimes(2);
      expect(mockedGetRepo.createQueryBuilder().where).toHaveBeenNthCalledWith(
        1,
        `"account"."clientId" = :clientId and "user"."status" = true`,
        { clientId: id }
      );
      expect(
        mockedGetRepo.createQueryBuilder().getMany
      ).toHaveBeenNthCalledWith(1);
    });

    test("Should return a list of accounts by Client", async () => {
      const id = 1;
      const accountsData = [
        {
          id: 1,
          account: "14b38161-47f6-4353-971e-424c072a13a2",
          balance: "0.00",
          nip: "4811",
          status: true,
        },
        {
          id: 2,
          account: "2669354f-e320-49c0-bf6c-406af6dce272",
          balance: "0.00",
          nip: "4190",
          status: true,
        },
      ];
      mockedGetRepo.createQueryBuilder = jest.fn().mockReturnValue({
        where: jest.fn().mockReturnThis(),
        leftJoinAndSelect: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockResolvedValue(accountsData),
      });
      const accounts = await AccountService.getAccountsByClient(id);
      expect(accounts).toEqual(accountsData);
      expect(
        mockedGetRepo.createQueryBuilder().leftJoinAndSelect
      ).toHaveBeenCalledTimes(2);
      expect(mockedGetRepo.createQueryBuilder().where).toHaveBeenNthCalledWith(
        1,
        `"account"."clientId" = :clientId and "user"."status" = true`,
        { clientId: id }
      );
      expect(
        mockedGetRepo.createQueryBuilder().getMany
      ).toHaveBeenNthCalledWith(1);
    });
  });
  describe("createAccount", () => {
    test("Create an new Account with the client id and product id provided", async () => {
      const data = {
        productId: 2,
        clientId: 2,
      };
      const returnData = {
        id: 8,
        account: "4656b6f6-4c61-48eb-8ae5-1f3ca64b2178",
        balance: "0.00",
        nip: "9530",
        status: true,
        productId: 2,
        clientId: 2,
      };
      mockedGetRepo.save.mockResolvedValue(returnData);
      const account = await AccountService.createAccount(data);
      expect(account).toEqual(returnData);
      expect(mockedGetRepo.save).toHaveBeenCalledTimes(1);
    });
  });
});

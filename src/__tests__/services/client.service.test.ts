import { getRepository } from "typeorm";
import { mocked } from "ts-jest/utils";
import * as ClientService from "../../services/client.service";

jest.mock("typeorm", () => {
  return {
    getRepository: jest.fn().mockReturnValue({
      createQueryBuilder: jest.fn(() => ({})),
      save: jest.fn(),
    }),
    PrimaryGeneratedColumn: jest.fn(),
    Column: jest.fn(),
    Entity: jest.fn(),
    OneToOne: jest.fn(),
    ManyToOne: jest.fn(),
    OneToMany: jest.fn(),
    JoinColumn: jest.fn(),
    CreateDateColumn: jest.fn(),
    UpdateDateColumn: jest.fn(),
  };
});

const mockedGetRepo = mocked(getRepository(<jest.Mock>{}));

beforeEach(() => {
  mockedGetRepo.createQueryBuilder.mockClear();
});

describe("ClientService", () => {
  describe("createClient", () => {
    test("Should return a object with the client, user and account data", async () => {
      const clientData = {
          name: "Rafa",
          lastname: "Flores",
          email: "rafa_flores@mail.com",
          phone: "8119876543",
          rfc: "GOFR700415F00",
          address: "Principal 1234, Centro",
      };
      const returnData = {
        client: {
          id: 1,
          name: "Rafa",
          lastname: "Flores",
          email: "rafa_flores@mail.com",
          phone: "8119876543",
          rfc: "GOFR700415F00",
          address: "Principal 1234, Centro",
        },
        user: {
          id: 1,
          username: "RafaFlores",
          token: null,
          status: true,
        },
        account: {
          id: 1,
          account: "ef1c680d-766e-4d0b-a957-69ff15316b64",
          balance: "0.00",
          nip: "4119",
          status: true,
          product: {
            id: 2,
          },
        },
      };
      mockedGetRepo.save.mockResolvedValue(returnData);
      const user = await ClientService.createClient(clientData);
      expect(user).toEqual(returnData);
      expect(mockedGetRepo.save).toHaveBeenCalledWith(clientData);
      expect(mockedGetRepo.save).toHaveBeenCalledTimes(1);
    });
  });
});

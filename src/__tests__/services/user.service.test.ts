import { getRepository } from "typeorm";
import { mocked } from "ts-jest/utils";
import * as UserService from "../../services/user.service";
import { IUser } from "../../models";

jest.mock("typeorm", () => {
  return {
    getRepository: jest.fn().mockReturnValue({
      createQueryBuilder: jest.fn(() => ({})),
      save: jest.fn(),
    }),
    PrimaryGeneratedColumn: jest.fn(),
    Column: jest.fn(),
    Entity: jest.fn(),
    OneToOne: jest.fn(),
    ManyToOne: jest.fn(),
    OneToMany: jest.fn(),
    JoinColumn: jest.fn(),
    CreateDateColumn: jest.fn(),
    UpdateDateColumn: jest.fn(),
  };
});

const mockedGetRepo = mocked(getRepository(<jest.Mock>{}));

beforeEach(() => {
  mockedGetRepo.createQueryBuilder.mockClear();
});

describe("UserService", () => {
  describe("createUser", () => {
    test("Create an new user with the client information provided", async () => {
      const userData : IUser = {
        client: {
          id: 4,
          name: 'Brenom',
          lastname: 'A.',
          email: 'abrenomB@mail.com',
          phone: '5509876543',
          rfc: 'AOFB700415Z1J',
          address: 'Secundaria 4321, Valle',
          accounts: undefined
        },
        username: 'BrenomAB',
        password: '12345678'
      };
      const returnData = {
        id: 4,
        username: 'BrenomAB',
        password: '$2b$10$hxYtJUp0J7T514IEO7Pjbulp5JdNN3oExeJvZJDcnLqm6l/PdS7pW',
        token: null,
        status: true,
        client: {
          id: 4,
          name: 'Brenom',
          lastname: 'A.',
          email: 'abrenomB@mail.com',
          phone: '5509876543',
          rfc: 'AOFB700415Z1J',
          address: 'Secundaria 4321, Valle',
          accounts: undefined
        },
        clientId: 4
      }
      mockedGetRepo.save.mockResolvedValue(returnData)
      const user = await UserService.createUser(userData);
      expect(user).toEqual(returnData)
      expect(mockedGetRepo.save).toHaveBeenCalledTimes(1)
    });
  });
});

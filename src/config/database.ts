import { ConnectionOptions } from "typeorm";
import { Client, User, Account, Product, Transaction, TransactionType, TransactionOperation, TransactionStatus } from "../models"

const config: ConnectionOptions = {
  type: "postgres",
  host: process.env.POSTGRES_HOST,
  port: Number(process.env.POSTGRES_PORT),
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  schema: process.env.POSTGRES_SCHEMA,
  entities: [Client, User, Account, Product, Transaction, TransactionType, TransactionOperation, TransactionStatus],
  synchronize: true,
};

export default config;